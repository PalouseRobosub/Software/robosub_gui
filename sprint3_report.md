# Sprint 3 Report (10/07/21 - 11/11/2021)
 
## What's New (User Facing)
 * IMU (roll, pitch, yaw), depth, and hydrophone (x, y, z) sensor data is displayed in the UI.
 * UI was cleaned and refactored to proportionally fit the data in the bottom left box.
 
## Work Summary (Developer Facing)
This sprint our team gathered sensor data from the submarine, improved the layout and displayed this data on the gui as well. We learned how to communicate with the submarine and this was vital to the development of the gui. Also we have a clearer vision of what the final product will now look like.
 
## Unfinished Work
We completed all of the necessary work that was required by our clients. They required the IMU, depth, and hydrophone data to be displayed in the raw sensor data section of our UI. The only work we did not finish was the acceleration sensor data, which they said was optional and not as important as the other three. We decided we would include this at a later point, when we add more features and visualizations to the UI.
 
## Completed Issues/User Stories
Here are links to the issues that we completed in this sprint:
 
 * https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/issues/12
 * https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/issues/3
 
 Reminders (Remove this section when you save the file):
  * Each issue should be assigned to a milestone
  * Each completed issue should be assigned to a pull request
  * Each completed pull request should include a link to a "Before and After" video
  * Explanation video:
  * https://www.dropbox.com/s/h5pi1f0v0zbiye6/zoom_0.mp4?dl=0
  * Optimal layout. Had issues while recording the screen and audio at the same time, but the application itself works fine: https://emailwsu-my.sharepoint.com/personal/kyle_k_martin_wsu_edu/Documents/Attachments/2021-11-11%2017-31-49.mkv
  * Each issue should be assigned story points using a label
  * Story points contribution of each team member should be indicated in a comment
 
 ## Incomplete Issues/User Stories
 Here are links to issues we worked on but did not complete in this sprint:
 
 * https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/issues/4 
We did not have enough time to implement this feature
 * https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/issues/5 This was not a priority issue for this sprint
 * https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/issues/7 This was not a priority issue for this sprint
 
 Examples of explanations (Remove this section when you save the file):
  * "We ran into a complication we did not anticipate (explain briefly)."
  * "We decided that the feature did not add sufficient value for us to work on it in this sprint (explain briefly)."
  * "We could not reproduce the bug" (explain briefly).
  * "We did not get to this issue because..." (explain briefly)
 
## Code Files for Review
Please review the following code files, which were actively developed during this sprint, for quality:
 * [main.py](https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/blob/main/main.py)
 * [sensor.py](https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/blob/main/sensor.py)
 
## Retrospective Summary
Here's what went well:
  * Importing and displaying sensor data from the ros2 simulator.
  * Refactoring and cleaning the UI.
  * Setting up tests for the sensor data.
 
Here's what we'd like to improve:
   * Add further to the UI, making it look more like a professional application.
   * Include more tests to ensure the sensor data readings are accurate as we begin to add visualizations to the UI.
 
Here are changes we plan to implement in the next sprint:
   * Include a terminal
   * Camera display
   * Begin adding visualizations, such as graphs or vectors to the UI.
