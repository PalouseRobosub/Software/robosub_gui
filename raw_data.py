from tkinter import *

class rawDataClass:
    def __init__(self,root):
        self.rawData = Canvas(root, bg="white",height = 800/2, width = 1200/2 )
        self.depthText = self.rawData.create_text(600/2, 260/2+75, text=0.0)
        self.rollText = self.rawData.create_text(600/2, 100/2+75, text=0.0)
        self.pitchText = self.rawData.create_text(600/2, 130/2+75, text=0.0)
        self.yawText = self.rawData.create_text(600/2, 160/2+75, text=0.0)
        
        self.hydrophoneText_x = self.rawData.create_text(600/2, 390/2+75, text=0.0)
        self.hydrophoneText_y = self.rawData.create_text(600/2, 420/2+75, text=0.0)
        self.hydrophoneText_z = self.rawData.create_text(600/2, 450/2+75, text=0.0)

    def get_raw_Data(self):
        return self.rawData
    
    def get_depthText(self):
        return self.depthText
    
    def set_depthText(self,depth):
        self.rawData.itemconfig(self.depthText, text = str(depth)[:-10])
        
    def get_roll_text(self):
        return self.rawData
    
    def set_roll_text(self,roll):
        self.rawData.itemconfig(self.rollText, text = str(roll)[:-10])
        
    def get_pitchText(self):
        return self.pitchText
    
    def set_pitchText(self,pitch):
        self.rawData.itemconfig(self.pitchText, text = str(pitch)[:-10])
    
    def get_yawText(self):
        return self.yawText

    def set_yawText(self,yaw):
        self.rawData.itemconfig(self.yawText, text = str(yaw)[:-10])
        
    def get_hydrophoneText_x(self):
        return self.hydrophoneText_x

    def set_hydrophoneText_x(self,x):
        self.rawData.itemconfig(self.hydrophoneText_x, text = str(x)[:-10])

    def get_hydrophoneText_y(self):
        return self.hydrophoneText_y
    
    def set_hydrophoneText_y(self,y):
        self.rawData.itemconfig(self.hydrophoneText_y, text = str(y)[:-10])
        
    def get_hydrophoneText_z(self):
        return self.hydrophoneText_z
    
    def set_hydrophoneText_z(self,z):
        
        self.rawData.itemconfig(self.hydrophoneText_z, text = str(z)[:-10])

    def createWindow(self, root):
        
        self.rawData.grid(row = 1, column= 0, sticky = N+S+E+W)

        self.rawData.create_text(600/2, 20/2+75, text="RAW DATA")

        # Print label and Depth value.
        self.rawData.create_text(450/2, 260/2+75, text="depth: ")

        # Print label and IMU values (roll, pitch, yaw).
        self.rawData.create_text(400/2, 70/2+75, text="IMU")
        self.rawData.create_text(450/2, 100/2+75, text="roll: ")
        self.rawData.create_text(450/2, 130/2+75, text="pitch: ")
        self.rawData.create_text(450/2, 160/2+75, text="yaw: ")


        # Print label and hydrophone value.
        self.rawData.create_text(450/2, 360/2+75, text="HYDROPHONE")
        self.rawData.create_text(450/2, 390/2+75, text="x: ")
        self.rawData.create_text(450/2, 420/2+75, text="y: ")
        self.rawData.create_text(450/2, 450/2+75, text="z: ")
 