'''
    File name: sensor.py
    Author: Ben Nachmanson, Kyle Martin, Pavel Braginskiy

'''
import os
from PIL import ImageTk
import PIL
from PIL import ImageDraw
from PIL import ImageColor
from PIL import ImageFont
from typing import Any, Callable, List
from rclpy.node import Node
import rclpy
from robosub_msgs.msg import Float32Stamped, Euler
from sensor_msgs.msg import Image
from geometry_msgs.msg import Vector3Stamped
from threading import Lock, Thread
import cv2
from cv_bridge import CvBridge
import numpy as np
import csv
import datetime
import time
from cv_bridge import CvBridge
import rclpy
from rclpy.node import Node 
#from main import
rclpy.init(args =None)
node = Node("sensor")

#print(node.get_topic_names_and_types())

# Writing to files
tempTime = datetime.datetime.now().strftime("%j-%H-%M-%S")

depthFile = open(f'./log/{tempTime}-depth.csv', 'w')
try:
    os.remove('./log/current-depth.csv')
except FileNotFoundError:
    pass
os.symlink(os.path.abspath(depthFile.name), './log/current-depth.csv')
depthWriter = csv.writer(depthFile)

imuFile = open(f'./log/{tempTime}-imu.csv', 'w')
try:
    os.remove('./log/current-imu.csv')
except FileNotFoundError:
    pass
os.symlink(os.path.abspath(imuFile.name), './log/current-imu.csv')
imuWriter = csv.writer(imuFile)

hydroFile = open(f'./log/{tempTime}-hydro.csv', 'w')
try:
    os.remove('./log/current-hydro.csv')
except FileNotFoundError:
    pass
os.symlink(os.path.abspath(hydroFile.name), './log/current-hydro.csv')
hydroWriter = csv.writer(hydroFile)

_depth: float = None
_depth_callbacks: List[Callable[[float], Any]] = []
_roll: float = None
_pitch: float = None
_yaw: float = None
_roll_callbacks: List[Callable[[float], Any]] = []
_pitch_callbacks: List[Callable[[float], Any]] = []
_yaw_callbacks: List[Callable[[float], Any]] = []

_x: Vector3Stamped = None
_y: Vector3Stamped = None
_z: Vector3Stamped = None
_hydrophone_callbacks_x: List[Callable[[Vector3Stamped], Any]] = []
_hydrophone_callbacks_y: List[Callable[[Vector3Stamped], Any]] = []
_hydrophone_callbacks_z: List[Callable[[Vector3Stamped], Any]] = []

_leftimage: Image = None
_leftimage_callbacks: List[Callable[[Image], Any]] = []

# def _depth_listener(msg: Float32Stamped):
#     global _depth
#     _depth = msg.data
#     for i in _depth_callbacks:
#         i(_depth)


def _imu_listener(msg: Euler):
    global _roll, _pitch, _yaw
    # roll, pitch, yawn
    _roll = msg.roll
    _pitch = msg.pitch
    _yaw = msg.yaw
    for i in _roll_callbacks:
        i(_roll)
    for i in _pitch_callbacks:
        i(_pitch)
    for i in _yaw_callbacks:
        i(_yaw)
    imuWriter.writerow((datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3], _roll, _pitch, _yaw))

def _leftimage_listener(msg: Image):
    global _leftimage
    # Need to find type of msg
    _leftimage = msg
    if msg is not None:
        left_image_callback(_leftimage)

    for i in _leftimage_callbacks:

        i(_leftimage)
        
def _depth_listener(msg: Float32Stamped):
    global _depth
    _depth = msg.data
    depthWriter.writerow((datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3], _depth))

    for i in _depth_callbacks:
        i(_depth)

def _hydrophone_listener(msg: Vector3Stamped):
    global _x, _y, _z

    _x = msg.vector.x
    _y = msg.vector.y
    _z = msg.vector.z
    
    for i in _hydrophone_callbacks_x:
        i(_x)
    for i in _hydrophone_callbacks_y:
        i(_y)
    for i in _hydrophone_callbacks_z:
        i(_z)
    hydroWriter.writerow((datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3],_x,_y,_z))


node.create_subscription(Float32Stamped, "depth", _depth_listener,1)

node.create_subscription(Euler, "/pretty/orientation", _imu_listener, 1)

# node.create_subscription(Image_msg, "/left/image_raw", _leftimage_listener, 1)

node.create_subscription(Vector3Stamped, "/hydrophones/bearing", _hydrophone_listener, 1)


node.create_subscription(Image, "/left/image_raw",_leftimage_listener, 1)


def left_image_callback(msg):
    pass

#Vector3Stamped


_listen_thread =  Thread(target=rclpy.spin, args=(node,))
_listen_thread.daemon = True
_listen_thread.start()



def set_depth_callback(callback: Callable[[float], Any]):
    _depth_callbacks.append(callback)

def get_depth() -> float:
    global _depth
    return _depth

def set_roll_callback(callback: Callable[[float], Any]):
    _roll_callbacks.append(callback)

def set_pitch_callback(callback: Callable[[float], Any]):
    _pitch_callbacks.append(callback)

def set_yaw_callback(callback: Callable[[float], Any]):
    _yaw_callbacks.append(callback)

def get_roll() -> float:
    global _roll
    return _roll

def get_pitch() -> float:
    global _pitch
    return _pitch

def get_yaw() -> float:
    global _yaw
    return _yaw

def set_leftimage_callback(callback: Callable[[Image],Any]):
    _leftimage_callbacks.append(callback)
	
def set_hydrophone_callback_x(callback: Callable[[Vector3Stamped], Any]):
    _hydrophone_callbacks_x.append(callback)

def set_hydrophone_callback_y(callback: Callable[[Vector3Stamped], Any]):
    _hydrophone_callbacks_y.append(callback)

def set_hydrophone_callback_z(callback: Callable[[Vector3Stamped], Any]):
    _hydrophone_callbacks_z.append(callback)

def get_hydrophone_x() -> Vector3Stamped:
    global _x
    return _x
def get_hydrophone_y() -> Vector3Stamped:
    global _y
    return _y
def get_hydrophone_z() -> Vector3Stamped:
    global _z
    return _z

# def get_leftimage() -> Image:
#     global _leftimage
#     return _leftimage
