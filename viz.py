from tkinter import Canvas
from PIL import Image, ImageTk
from sensor import set_roll_callback, set_pitch_callback, set_yaw_callback


_canvas: Canvas

_roll_image: Image.Image = Image.open('images/front.png').convert("RGBA")
_pitch_image: Image.Image = Image.open('images/side.png').convert("RGBA")
_yaw_image: Image.Image = Image.open('images/top.png').convert("RGBA")

def _roll_callback(roll: float):
    try:
        _canvas.delete(_roll_callback.canvas_obj)
    except:
        return
    _roll_callback.tk_image = ImageTk.PhotoImage(_roll_image.rotate(-roll, expand=True, resample=Image.BICUBIC))
    _roll_callback.canvas_obj = _canvas.create_image(_canvas.winfo_reqwidth()/2, _canvas.winfo_reqheight()/4 * _roll_callback.position, image=_roll_callback.tk_image)


def _pitch_callback(pitch: float):
    try:
        _canvas.delete(_pitch_callback.canvas_obj)
    except:
        return
    _pitch_callback.tk_image = ImageTk.PhotoImage(_pitch_image.rotate(-pitch, expand=True, resample=Image.BICUBIC))
    _canvas.delete(_pitch_callback.canvas_obj)
    _pitch_callback.canvas_obj = _canvas.create_image(_canvas.winfo_reqwidth()/2, _canvas.winfo_reqheight()/4 * _pitch_callback.position, image=_pitch_callback.tk_image)

def _yaw_callback(yaw: float):
    try:
        _canvas.delete(_yaw_callback.canvas_obj)
    except:
        return
    _yaw_callback.tk_image = ImageTk.PhotoImage(_yaw_image.rotate(yaw, expand=True, resample=Image.BICUBIC))
    _canvas.delete(_yaw_callback.canvas_obj)
    _yaw_callback.canvas_obj = _canvas.create_image(_canvas.winfo_reqwidth()/2, _canvas.winfo_reqheight()/4 * _yaw_callback.position, image=_yaw_callback.tk_image)

def init(canvas: Canvas):
    global _canvas
    _canvas = canvas
    
    for i in [(_roll_callback, _roll_image, 1), (_pitch_callback, _pitch_image, 2), (_yaw_callback, _yaw_image, 3)]:
        i[0].position = i[2]
        i[0].tk_image = ImageTk.PhotoImage(i[1])
        i[0].canvas_obj = _canvas.create_image(_canvas.winfo_reqwidth()/2, _canvas.winfo_reqheight()/4 * i[2], image=i[0].tk_image)
    
    set_roll_callback(_roll_callback)
    set_pitch_callback(_pitch_callback)
    set_yaw_callback(_yaw_callback)

    

