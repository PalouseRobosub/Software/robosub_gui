from tkinter import *
import os
from PIL import ImageTk,Image

class cameraClass:
    def __init__(self,root):
        self.root = root
        self.cwd = os.getcwd()
        self.im = Image.open(os.path.abspath(os.getcwd()) + "/sub.png")
        self.ph = ImageTk.PhotoImage(self.im)
        self.panel = Label(root, image=self.ph)
        self.panel.photo = self.ph
    
    
    def set_left_image(self,imgtk: PhotoImage,msg):
        self.ph = imgtk
        self.panel = Label(self.root, image=imgtk)
        self.panel.photo = imgtk
        self.createWindow(self.root)

    def createWindow(self, root):
        self.panel.grid(row = 0, column= 1, sticky = N+S+E+W)