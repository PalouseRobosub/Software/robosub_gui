# Team Robosub
We are building a Graphical User Interface for a robot submarine.
One-sentence description of the project.
TODO: A 20-second elevator pitch of your project - its core idea summarized in one sentence.

## Additional information about the project.

The RoboSub uses a combination of sensors including cameras, depth sensors, hydrophones, an inertial measurement unit, and thrusters in order to navigate its environment autonomously. Our job will be to retrieve this sensor data and display them in a GUI.

## Installation
### Prerequisites
See https://robosub.eecs.wsu.edu/wiki/cs/getting_started/start, which has instructions for installing all necessary robosub packages.

`sudo apt-get install python3-matplotlib tk-dev libpng-dev libffi-dev dvipng texlive-latex-base python3-pip  python3-pil.imagetk python3-opencv`

`sudo python3 -m pip install -U matplotlib`

## Add-ons

List which add-ons are included in the project, and the purpose each add-on serves in your app.

## Installation Steps
TODO: Describe the installation process (making sure you mention bundle install). Instructions need to be such that a user can just copy/paste the commands to get things set up and running.

## Functionality
TODO: Write usage instructions. Structuring it as a walkthrough can help structure this section, and showcase your features.

## Known Problems
TODO: Describe any known issues, bugs, odd behaviors or code smells. Provide steps to reproduce the problem and/or name a file or a function where the problem lives.

## Contributing
TODO: Leave the steps below if you want others to contribute to your project.

## Fork it!
Create your feature branch: git checkout -b my-new-feature
Commit your changes: git commit -am 'Add some feature'
Push to the branch: git push origin my-new-feature
Submit a pull request :D
Additional Documentation
TODO: Provide links to additional documentation that may exist in the repo, e.g., * Sprint reports * User links

