#!/usr/bin/env python3

from tkinter import Canvas
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from threading import Lock
from datetime import datetime, timedelta
from typing import Any, Callable, List, Tuple
from sensor import set_depth_callback
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import math

_canvas : Canvas = None
_plot : FigureCanvasTkAgg = None

_data : List[Tuple[datetime, float]] = []
_data_lock : Lock = Lock()

_fig : Figure = plt.figure()

sample_resolution : timedelta = timedelta(seconds = .03) # todo make configurable
max_samples : int = 300 # todo make configurable

def draw_graph() -> mpl.figure.Figure:
    _fig.clear()
    ax : Axes = _fig.add_subplot()
    with _data_lock:
        if len(_data) == 0:
            ax.plot([],[])
            return _fig
        last_instant = _data[-1][0]
        x = list(map(lambda i: (i[0] - last_instant).total_seconds(), _data))
        y = list(map(lambda i: i[1], _data))
    
    ax.plot(x, y)
    ax.set_title('UAV Depth')
    ax.set_xlabel('Time ago (seconds)')
    ax.set_ylabel('Depth (meters)')
    ax.set_ylim(top=0, bottom=math.floor(min(y)) or -1)
    return _fig
    
def _depth_callback(depth: float):
    now = datetime.now()
    with _data_lock:
        if len(_data) > 0 and now - _data[-1][0] < sample_resolution:
            return
        _data.append((now, depth))
        if len(_data) > max_samples:
            _data.pop(0)
    if _canvas:
        draw_graph()
        global _plot
        if _plot is None:
            _plot = FigureCanvasTkAgg(_fig, master = _canvas)
        _plot.draw()
        _plot.get_tk_widget().pack()
        




def set_plot_canvas(canvas: Canvas):
    global _canvas
    _canvas = canvas

set_depth_callback(_depth_callback)

if __name__ == '__main__':
    from time import sleep
    sleep(10)
    draw_graph().savefig('plot.png')