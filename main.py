#!/usr/bin/env python3
'''
    File name: main.py
    Author: Ben Nachmanson, Kyle Martin, Pavel Braginskiy

'''

# Import module
from tkinter import *
import os
from PIL import ImageTk,Image

import cv2
import csv
from numpy import fix
from cv_bridge import CvBridge
import rclpy
from rclpy.node import Node 
global imgtk
from sensor import depthFile,hydroFile,imuFile

#import matplotlib; matplotlib.use('TkAgg')
from matplotlib.figure import Figure
from plot import set_plot_canvas

from sensor import get_hydrophone_z, set_depth_callback, set_hydrophone_callback_x,set_hydrophone_callback_y,set_hydrophone_callback_z, set_roll_callback, set_pitch_callback, set_yaw_callback,  set_leftimage_callback, Euler

from subprocess import Popen

from raw_data import rawDataClass
from camera import cameraClass
import viz

#refresh pending data
#rclpy.spin_once(node)

os.path.abspath(os.getcwd())
root = Tk()
root.title("RoboSub")

#trying to add an icon... to top left
root.iconbitmap('@sub.xbm')

#root.geometry("500x500")
#root.resizable(True, True)

# Information for menu bar (File, Settings)
menu_bar = Menu(root)
def clickedExampleItem(menu):
    menu.entryconfigure(1, label="Clicked!")
    
def clickedShowAll(menu):
    menu.entryconfigure(1, label="Show All ✓")

def clickedShowPlot(menu):
    print(menu)
    menu.entryconfigure(2, label="Show Only Depth Plot ✓")


# Specify Grid
Grid.rowconfigure(root,1,weight=1)
Grid.columnconfigure(root,1,weight=1)

#BUTTONS
#CmdButton


terminal_process: Popen = None
terminal_process_ctrl: Popen = None

def on_cmd_click():
	global terminal_process
	if terminal_process is None or terminal_process.poll() is not None:
		terminal_process = Popen('xterm')
		print(terminal_process.poll())
	else:
		terminal_process.kill()



def ctrl_click():
	global terminal_process_ctrl
	if terminal_process_ctrl is None or terminal_process_ctrl.poll() is not None:
		terminal_process_ctrl = Popen(['xterm','-e','ros2 run robosub keyboard_control'])
		print(terminal_process_ctrl.poll())
	else:
		terminal_process_ctrl.kill()


global popOutImuData

def show_plot():
   
    # global popOutImuData
    # window = Toplevel()
    # label = Label(window, text="plot")
    # window.geometry("300x300")

    # label.pack(fill='x', padx=50, pady=5)
    # popOutImuData = Canvas(window, bg="white",height = 300, width = 300)
    # popOutImuData.create_text(300, 50, text="HELLO WORLD", fill="black", font=('Helvetica 15 bold'))

    # popOutImuData.pack()
	
	
	# Creting the Plot window
    #imuData.grid(row = 0, column= 0, sticky=N+S+E+W)
    pass

    
# Creating the Raw Data Window
r1 = rawDataClass(root)
r1.createWindow(root)

# Creating the Camera window
c1 = cameraClass(root)
c1.createWindow(root)

# Creting the Plot window
imuData = Canvas(root, bg="white",height = 700/2, width = 1200/2 )
imuData.grid(row = 0, column= 0, sticky=N+S+E+W)

CmdButton = Button(root,text="CMD", command=on_cmd_click)
CmdButton.grid(row=2,column=0,sticky="W")

#Controller Button

ControllerButton = Button(root, text = "CTLR On/Off", command = ctrl_click)
ControllerButton.grid(row=2,column=1,sticky = "W")

imuVisual = Canvas(root, bg="white",height = 800/2, width = 1200/2 )
imuVisual.grid(row = 1, column = 1, sticky = N+S+E+W)

viz.init(imuVisual)


file_menu = Menu(menu_bar, tearoff=False)
settings_menu = Menu(menu_bar,tearoff=False)
file_menu.add_command(label="To be implemented...", command=lambda: clickedExampleItem(file_menu))
settings_menu.add_command(label="To be implemented...", command=show_plot)

menu_bar.add_cascade(label="File", menu=file_menu)
menu_bar.add_cascade(label="Settings", menu =settings_menu)
root.config(menu=menu_bar)

# initializing the image

#Printing data

def depth_callback(depth: float):
    r1.set_depthText(depth)
    #depthFile.flush()

def roll_callback(roll: float):
 	r1.set_roll_text(roll)

def pitch_callback(pitch: float):
	# Printing text to GUI.
	r1.set_pitchText(pitch)
	
def yaw_callback(yaw: float):
	# Printing text to GUI.
	r1.set_yawText(yaw)

def hydrophone_callback_x(x: float):
	# Printing text to GUI.
	r1.set_hydrophoneText_x(x)
	
def hydrophone_callback_y(y: float):
		# Printing text to console.
	# print(imu)

	# Printing text to GUI.
	r1.set_hydrophoneText_y(y)
	#.rawData.itemconfig(hy)
def hydrophone_callback_z(z: float):
	# print(imu)

	# Printing text to GUI.
	r1.set_hydrophoneText_z(z)

images = []
def left_image_callback(msg):
    global imgtk
    #camera.itemconfig()
    bridge = CvBridge()
    img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
    
    im = Image.fromarray(img).resize((600, 350), Image.BICUBIC)
    imgtk = ImageTk.PhotoImage(im)
    #images.append(ImageTk.PhotoImage(im))
    
    c1.set_left_image(imgtk,msg)
    #camera.create_image(300,200, image = imgtk)
    #camera.pack()
set_leftimage_callback(left_image_callback)
set_plot_canvas(imuData)
set_depth_callback(depth_callback)
set_roll_callback(roll_callback)
set_pitch_callback(pitch_callback)
set_yaw_callback(yaw_callback)
set_hydrophone_callback_x(hydrophone_callback_x)
set_hydrophone_callback_y(hydrophone_callback_y)
set_hydrophone_callback_z(hydrophone_callback_z)

# Execute tkinter
if __name__ == '__main__':
	root.mainloop()
 
	depthFile.close()
	imuFile.close()
	hydroFile.close()