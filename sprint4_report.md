# Sprint 4 Report (11/11/21 - 12/09/2021)
 
## What's New (User Facing)
 * Updated the GUI layout
 * Added an integrated xterm terminal
 
## Work Summary (Developer Facing)
In this sprint we added an integrated xterm terminal to our GUI. We decided to make this a button for now, which when pressed, will open the terminal as a window. To close the terminal, you simply press the button again. In the next sprint we plan on making a separate window for the terminal itself. This will be located at the bottom of the program, similar to how it is displayed on applications such as Visual Studio Code.
 
## Unfinished Work
For this sprint we wanted to integrate the camera but we ended up not having enough time. We previously initiated functions for the left camera, but had difficulties getting it to display to the window. We determined that it would be best to save this for the next sprint, when we would have more time to debug the problem with our clients.
 
## Completed Issues/User Stories
Here are links to the issues that we completed in this sprint:
 
 * https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/issues/4
 * https://www.dropbox.com/s/lf8kt57tfth2l1c/2021-12-09%2017-33-01.mkv?dl=0
 ## Incomplete Issues/User Stories
 Here are links to issues we worked on but did not complete in this sprint:
 
 * https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/issues/7 
We did not finish this issue because we did not have enough time.
 
## Code Files for Review
Please review the following code files, which were actively developed during this sprint, for quality:
 *[main.py](https://github.com/WSUCptSCapstone-Fall2021Spring2022/robosub-guiapp/blob/main/main.py)
 
## Retrospective Summary
Here's what went well:
  * We got the terminal to work.
  * Good team communication.
 
Here's what we'd like to improve:
   * Better time management. 
 
Here are changes we plan to implement in the next sprint:
   * Get the Camera to work on the gui.
   * Integrate the Terminal as its own separate window at the bottom of the GUI.
 
