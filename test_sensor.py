'''
    File name: test_sensor.py
    Author: Ben Nachmanson, Kyle Martin, Pavel Braginskiy

'''
from sensor import get_depth

def test_get_depth():
    assert isinstance(get_depth(), float)